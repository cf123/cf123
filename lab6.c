#include<stdio.h>
int main()
{
    int a[5][3],i,j,large;
    printf("please enter the marks of the students in the same order subject-1, subject-2,subject-3\n");
    for(i=0;i<5;i++)
    {
        printf("enter the marks of student %d:\n",i+1);
        for(j=0;j<3;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("the entered marks are:\n");
    for(i=0;i<5;i++)
    {
        for(j=0;j<3;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    for(j=0;j<3;j++)
    {
        large=0;
        for(i=0;i<5;i++)
        {
            if(a[i][j]>large)
            {
                large=a[i][j];
            }
        }
        printf("the highest marks of subject-%d is %d\n",j+1,large);
    }
    return 0;
}