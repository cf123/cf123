#include<stdio.h>
#include<math.h>
int main()
{
float a,b,c;
printf("enter the coefficients of the quadratic expression\n");
scanf("%f%f%f",&a,&b,&c);
float dis;
dis=(b*b)-(4*a*c);
if(dis==0)
{
printf("the roots are equal\n");
float r1,r2;
r1=-b/(2*a);
r2=r1;
printf("the roots of the equation are %f and %f\n",r1,r2);
}
else if(dis>0)
{
printf("the roots are real\n");
float r1,r2;
r1=(-b+sqrt(dis))/2*a;
r2=(-b-sqrt(dis))/2*a;
printf("the roots are %f and %f\n",r1,r2);
}
else 
{
printf("the roots are imaginary\n");
printf("the roots are %f+i(%f) and %f-i(%f)\n",-b/(2*a),sqrt(fabs(dis))/(2*a),-b/(2*a),sqrt(fabs(dis))/(2*a));
}
return 0;
}

