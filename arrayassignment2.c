#include<stdio.h>
int main()
{
    int a[50],n,mid,beg,end,i,ele,p,flag;
    printf("enter the size of the array(max:50):\n");
    scanf("%d",&n);
    printf("enter the array elements(in ascending order only):\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("enter the element to be searched:\n");
    scanf("%d",&ele);
    flag=0;
    beg=0;
    end=n-1;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(a[mid]==ele)
        {
            p=mid;
            flag=1;
        }
        else if(a[mid]<ele)
        {
            beg=mid+1;
        }
        else
        {
            end=mid-1;
        }
    }
    if(flag==1)
    {
        printf("the element searched %d is found at position:%d int the array\n",ele,p);
    }
    else
    {
        printf("the element %d is not found in the array\n",ele);
    }
    return 0;
}
