#include<stdio.h>
int main()
{
	int ar[10],n,i,large,small,temp,l,s;
	printf("enter the size of the array\n");
	scanf("%d",&n);
	printf("enter the elements of the array\n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&ar[i]);
	}
	printf("the  array before interchanging is\n");
	for(i=0;i<n;i++)
	{
		printf("%d\t",ar[i]);
	}
	printf("\n");
	large=ar[0];
	for(i=1;i<n;i++)
	{
		if(large<ar[i])
		{
			large=ar[i];
			l=i;
		}
	}
	small=ar[0];
	for(i=1;i<n;i++)
	{
		if(small>ar[i])
		{
			small=ar[i];
			s=i;
		}
	}
	temp=ar[s];
	ar[s]=ar[l];
	ar[l]=temp;
	printf("the array after interchanging is \n");
	for(int i=0;i<n;i++)
	{
		printf("%d\t",ar[i]);
	}
	return 0;
}
