#include<stdio.h>
void swap(int *a, int *b)
{
	int temp;
	temp = *a;
	*a = *b;
	*b = temp;
}
int main()
{
	int a,b;
	printf("enter the two integers to be swapped\n");
	scanf("%d%d",&a,&b);
	printf("integers before swapping are a = %d and b = %d\n" ,a , b);
	swap(&a, &b);
	printf("the integers after interchanging are a = %d and b = %d\n", a , b);
	return 0;
}


